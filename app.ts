import { Component } from "@angular/core";
import { NgModule,OnInit } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule,Http,Response } from "@angular/http";
import {RouterModule,Routes,ActivatedRoute,Router } from "@angular/router";
import { Injectable,Inject } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { FormsModule }  from '@angular/forms';
import {LocationStrategy,HashLocationStrategy,APP_BASE_HREF,Location} from "@angular/common";
import 'rxjs/Rx';


@Component({ 
     selector: "spotify-search",
     template: `
          <router-outlet></router-outlet>
          `
})
class SpotifySearch {
     query:string;
}


@Injectable()
class SpotifyService {
     static URL_BASE: string = "https://api.spotify.com/v1";
     constructor(private http:Http) { }
     query(url: string, params?: Array<string>): Observable<any> {
          let queryUrl: string = `${SpotifyService.URL_BASE}${url}`;
          if(params) {
               queryUrl=`${queryUrl}?${params.join('&')}`;
               }
          return this.http.request(queryUrl).map((res: any) => res.json());
     }
     search(query: string,type: string): Observable<any[]> {
          return this.query('/search',[`q=${query}`,`type=${type}`]);
     }
     searchTrack(query: string): Observable<any[]> {
          return this.search(query,'track');
     }
     getTrack(id: string): Observable<any[]> {
          return this.query(`/tracks/${id}`);
     }
     getArtist(id: string): Observable<any[]> {
          return this.query(`/artist/${id}`);
     }
     getAlbum(id: string): Observable<any[]> {
          return this.query(`/albums/${id}`);
     }
}
@Component({
     selector: 'search',
     template: `
          <h1>search</h1>
          <p>
               <input type="text" #newquery (keydown.enter)="submit(newquery.value)" [value]="query" >
               <button (click)="submit(newquery.value)">search</button>
          </p>
          <div *ngIf="results">
               <div *ngIf="!results.length">
                    no track found!
               </div>
          <div *ngIf="results.length">
               <h1>results</h1>
               <div class="row">
                    <div class="col-sm-6 col-md-4" *ngFor="let eachRes of results">
                    <div class="thumbnail">
                         <img class="img img-responsive" src="{{eachRes.album.images[0].url}}">
                         <div class="caption">
                              <a [routerLink]="['/artist',eachRes.artists[0].id]"> {{ eachRes.artists[0].name }} </a>
                              <br>
                              <a [routerLink]="['/track',eachRes.id]"> {{ eachRes.name }}</a>
                              <br>
                              <a [routerLink]="['/albums',eachRes.album.id]"> {{ eachRes.album.name }}</a>
                         </div>
                    </div>
                    </div>
               </div>
          </div>
          `
})
class SearchCom implements OnInit {
     query: string;
     results: Object;
     constructor(private spoti: SpotifyService,
               private router: Router,
               private route: ActivatedRoute) {
          this.route.queryParams.subscribe(params => { this.query = params['query'] || ''; });
          }
          ngOnInit() {
               this.search();
          }
          submit(query: string) {
               this.router.navigate(['search'],{ queryParams: { query: query }})
               .then(_ => this.search() );
          }
          search(): void {
               console.log("this.query",this.query);
               if(!this.query) {
                    return;
               }
               this.spoti.searchTrack(this.query).subscribe((res: any) => this.renderResults(res));
          }
          renderResults(res: any): void {
               this.results = null;
               if(res && res.tracks && res.tracks.items) {
                    this.results = res.tracks.items;
               }
          }
}
@Component({
     selector: 'trackk',
     template: `
          <div *ngIf="track">
               <h1>{{ track.name }}</h1>
               <img src=" track.album.images[1].url" }}>
               <audio controls src="{{ track.preview_url }}"></audio>
               <p><a href (click)="back()">Back</a></p>
               </div>
               `
          })
class TrackCom implements OnInit {
     id: string;
     track: Object;
     constructor(private route: ActivatedRoute, private spotify: SpotifyService, public location: Location) {
          route.params.subscribe(params => { this.id = params['id']; });
     }
     ngOnInit(): void {
          this.spotify.getTrack(this.id).subscribe((res: any) => this.renderTrack(res));
     }
     back(): void {
          this.location.back();
     }
     renderTrack(res: any): void {
          this.track = res;
     }
}
@Component({
     selector: "albums",
     template: `
          <div *ngIf="album">
               <h1>{{ album.name }}</h1>
               <h2>{{ album.artists[0].name }}</h2>
               <img src="{{ album.images[1].url }}">
               <h3>Tracks</h3>
               <ol>
                    <li *ngFor="let t of album.tracks.items">
                         <a [routerLink]="['/tracks',t.id]">
                         {{ t.name }}
                         </a>
                    </li>
               </ol>
               <p><a (click)="back()">Back</a></p>
          </div>
          `
})
class AlbumCom implements OnInit {
     id: string;
     album: Object;
     constructor(private route: ActivatedRoute,
               private spotify: SpotifyService,
               private location: Location) {
          route.params.subscribe(params => { this.id = params['id']; })
     }
     ngOnInit(): void {
          this.spotify.getAlbum(this.id).subscribe((res: any) => this.renderAlbum(res));
     }
     back(): void {
          this.location.back();
     }
     renderAlbum(res: any): void {
          this.album = res;
     }
}

@Component({
  selector: 'artist',
  template: `
  <div *ngIf="artist">
    <h1>{{ artist.name }}</h1>

    <p>
      <img src="{{ artist.images[0].url }}">
    </p>

    <p><a href (click)="back()">Back</a></p>
  </div>
  `
})
export class ArtistCom implements OnInit {
  id: string;
  artist: Object;

  constructor(private route: ActivatedRoute, private spotify: SpotifyService,
              private location: Location) {
    route.params.subscribe(params => { this.id = params['id']; });
  }

  ngOnInit(): void {
    this.spotify
      .getArtist(this.id)
      .subscribe((res: any) => this.renderArtist(res));
  }

  back(): void {
    this.location.back();
  }

  renderArtist(res: any): void {
    this.artist = res;
  }
}

const  routes: Routes = [
          { path: '', redirectTo: 'search', pathMatch: 'full' },
          { path: 'search', component: SearchCom },
          { path: 'track/:id', component: TrackCom },
          { path: 'artist/:id', component: ArtistCom },
          { path: 'albums/:id', component: AlbumCom }
];

@NgModule({
     declarations: [TrackCom,AlbumCom,SpotifySearch,SearchCom,ArtistCom ],
     imports: [ BrowserModule,HttpModule,FormsModule,RouterModule.forRoot(routes)],
     bootstrap: [SpotifySearch],
     providers: [SpotifyService,{provide: APP_BASE_HREF, useValue:'/'},{provide: LocationStrategy, useClass: HashLocationStrategy}]
     })
class SpotifySearchModule{}
platformBrowserDynamic().bootstrapModule(SpotifySearchModule);
